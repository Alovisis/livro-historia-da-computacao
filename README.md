# Introdução a Engenharia de Computação

Este livro está sendo escrito pelos alunos do Curso de Engenharia da Computação da Universidade Tecnológica do Paraná. O livro é faz parte da disciplina de Introdução a Engenharia e os alunos que participaram da edição estão [listados abaixo](#Autores).

## Índice

1. [Surgimento das Calculadoras Mecânicas](capitulos/surgimento_das_calculadoras_mecanicas.md)
1. [Inteligencia_Artificial](https://gitlab.com/eduardofarfus1/livro-historia-da-computacao/-/blob/master/Capítulos/Inteligencia_Artificial.md)
1. [Evolução dos Computadores Pessoais e sua Interconexão]()
    - [Primeira Geração]()
1. [Computação Móvel]()
1. [Futuro]()


## Autores
Esse livro foi escrito por:

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/11526545/avatar.png?width=90)  | Eduardo Farfus   | eduardofarfus1  | [eduardofarfus@alunos.utfpr.edu.br](mailto:eduardofarfus@alunos.utfpr.edu.br)
| ![](https://gitlab.com/uploads/-/system/user/avatar/11467782/avatar.png?width=90)  | João Pedro | joao-domingos   | [joaodomingos@alunos.utfpr.edu.br](mailto:joaodomingos@alunos.utfpr.edu.br)
| ![](https://gitlab.com/uploads/-/system/user/avatar/11521036/avatar.png?width=90)  | Gabriel Alovisi  | Alovisis | [gabriel.alovisi1@gmail.com](mailto:gabriel.alovisi1@gmail.com)
| ![](https://gitlab.com/uploads/-/system/user/avatar/11492692/avatar.png?width=90)  | Matheus Avila  | matheusavila | [matheusavila@alunos.utfpr.edu.br](mailto:matheusavila@alunos.utfpr.edu.br)
| ![](https://gitlab.com/uploads/-/system/user/avatar/11527017/avatar.png?width=90)  | Matheus Lima  | matheusichiro |[matheusichiro@alunos.utfpr.edu.br](mailto:matheusichiro@alunos.utfpr.edu.br) 

